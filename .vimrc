let g:ale_fixers = {
\ 'javascript': ['standard'],
\}

let g:ale_linters = {
\ 'html': [],
\ 'javascript': ['standard'],
\}
