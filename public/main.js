const fragranceMassValue = document.getElementById('FragranceMass')
const fragrancePercentInput = document.getElementById('FragrancePercent')
const fragrancePercentValue = document.getElementById('FragrancePercentValue')
const lyeMassValue = document.getElementById('LyeMass')
const oilMassInput = document.getElementById('OilMass')
const oilMassValue = document.getElementById('OilMassValue')
const sodiumCitrateMassValue = document.getElementById('SodiumCitrateMass')
const sodiumCitratePercentInput = document.getElementById('SodiumCitratePercent')
const sodiumCitratePercentValue = document.getElementById('SodiumCitratePercentValue')
const superFatPercentInput = document.getElementById('SuperFatPercent')
const superFatPercentValue = document.getElementById('SuperFatPercentValue')
const waterMassValue = document.getElementById('WaterMass')
const waterPercentInput = document.getElementById('WaterPercent')
const waterPercentValue = document.getElementById('WaterPercentValue')

const coconutOilSap = 0.257

let fragrancePercent
let oilMass
let sodiumCitratePercent
let superFatPercent
let waterPercent

function recalculate () {
  waterMassValue.textContent = Math.round(waterPercent * oilMass)
  lyeMassValue.textContent = Math.round((coconutOilSap / 1.4025) * oilMass * (1 - superFatPercent))
  sodiumCitrateMassValue.textContent = Math.round(sodiumCitratePercent * oilMass)
  fragranceMassValue.textContent = Math.round(fragrancePercent * oilMass)
}

function setFragrancePercent (percentString) {
  fragrancePercent = parseInt(percentString, 10) * 0.01
  fragrancePercentValue.textContent = percentString
  recalculate()
}

function setOilMass (massString) {
  oilMass = parseInt(massString, 10)
  oilMassValue.textContent = massString
  recalculate()
}

function setSodiumCitratePercent (percentString) {
  sodiumCitratePercent = parseInt(percentString, 10) * 0.01
  sodiumCitratePercentValue.textContent = percentString
  recalculate()
}

function setSuperFatPercent (percentString) {
  superFatPercent = parseInt(percentString, 10) * 0.01
  superFatPercentValue.textContent = percentString
  recalculate()
}

function setWaterPercent (percentString) {
  waterPercent = parseInt(percentString, 10) * 0.01
  waterPercentValue.textContent = percentString
  recalculate()
}

oilMassInput.addEventListener('input', event => {
  setOilMass(event.target.value)
})
setOilMass(oilMassInput.value)

waterPercentInput.addEventListener('input', event => {
  setWaterPercent(event.target.value)
})
setWaterPercent(waterPercentInput.value)

superFatPercentInput.addEventListener('input', event => {
  setSuperFatPercent(event.target.value)
})
setSuperFatPercent(superFatPercentInput.value)

sodiumCitratePercentInput.addEventListener('input', event => {
  setSodiumCitratePercent(event.target.value)
})
setSodiumCitratePercent(sodiumCitratePercentInput.value)

fragrancePercentInput.addEventListener('input', event => {
  setFragrancePercent(event.target.value)
})
setFragrancePercent(fragrancePercentInput.value)
